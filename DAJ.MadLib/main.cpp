//Damian Johnson
//Mad Lib

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

char save;

string inputs[12];

string message[12]
{
	"an adjective: ",
	"a sport: ",
	"a place: ",
	"a person or character: ",
	"a verb: ",
	"a vehichle: ",
	"another place: ",
	"a noun: ",
	"another adjective: ",
	"a food: ",
	"a liquid: ",
	"a final adjective: "
};

string mltemp[13]
{
	"One day my ",
	" friend and I decided to go to the ",
	" game in ",
	".\nWe really wanted to see ",
	" play.\nSo we ",
	" in the ",
	" and headed down to ",
	" and bought some ",
	".\nWe watched the game and it was ",
	".\nWe ate some ",
	" and drank some ",
	".\nWe had a ",
	" time, and can't wait to go again."
};

void Print(string mltemp2[], string inputs2[])
{
	for (int i = 0; i < 12; i++)
	{
		cout << mltemp2[i] << inputs2[i];
	}
	cout << mltemp2[12];
}

void Save(string mltemp2[], string inputs2[], ostream& out)
{
	for (int i = 0; i < 12; i++)
	{
		out << mltemp2[i] << inputs2[i];
	}
	out << mltemp2[12];
}

int main()
{

	string temp;

	for (int i = 0; i < 12; i++)
	{
		cout << "Please enter in " << message[i];
		getline(cin, temp);
		inputs[i] = temp;
		cout << "\n";
	}
	Print(mltemp, inputs);
	string filePath = "MadLib.txt";
	ofstream ofs(filePath);

	cout << "\n\nwould you like to save your madlib? Y = yes - Any other key = no\n";
	cin >> save;
	if (save == 'Y' || save == 'y')
	{
		if (ofs)
		{
			Save(mltemp, inputs, ofs);
			cout << "Saved\n";
		}
	}
	else
		cout << "You MadLib wasn't saved. Press any key to close the program\n";

	ofs.close();

	(void)_getch();
	return 0;
}
